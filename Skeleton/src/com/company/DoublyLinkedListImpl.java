package com.company;

import java.util.Iterator;
import java.util.List;

public class DoublyLinkedListImpl<T> implements DoublyLinkedList<T> {

    private DoubleNode<T> head;
    private DoubleNode<T> tail;
    private int count = 0;

    public DoublyLinkedListImpl(List<T> data) {
        data.stream().forEach(e -> addFirst(e));
    }

    public DoublyLinkedListImpl() {
        this.count = 0;
        this.head = null;
        this.tail = null;
    }

    @Override
    public DoubleNode getHead() {
        return head;
    }

    @Override
    public DoubleNode getTail() {
        return tail;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void addLast(T value) {
        if (count == 0) {
            head = tail = new DoubleNode<T>(value, null, null);
        } else {
            tail.next = new DoubleNode<T>(value, tail, null);
            tail = tail.next;
        }
        count++;
    }

    @Override
    public void addFirst(T value) {
        if (count == 0) {
            head = tail = new DoubleNode<T>(value, null, null);
        } else {
            head.prev = new DoubleNode<T>(value, null, head);
            head = head.prev;
        }
        count++;
    }

    @Override
    public void insertBefore(DoubleNode node, T value) {
        if (node == null) {
            throw new NullPointerException();
        }
        if (node == head) {
            addFirst(value);
            return;
        }
        DoubleNode<T> newNode = new DoubleNode<>(value);
        newNode.setNext(node);
        newNode.setPrev(node.getPrev());
        node.getPrev().setNext(newNode);
        node.setPrev(newNode);
        count++;
    }

    @Override
    public void insertAfter(DoubleNode node, T value) {
        if (node == tail) {
            addLast(value);
            return;
        }
        DoubleNode<T> newNode = new DoubleNode<>(value);
        newNode.setPrev(node);
        newNode.setNext(node.getNext());
        node.setNext(newNode);
        node.getNext().setPrev(newNode);
        count++;
    }

    @Override
    public T removeFirst() {
       if (count == 0) {
           throw new IllegalArgumentException();
       }
       T result = head.getValue();
       head = head.getNext();
       count--;
       if (head != null) {
           head.setPrev(null);
       }
       if (count == 0) {
           tail = null;
       } else {
           head.setPrev(null);
       }
       return result;
    }

    @Override
    public T removeLast() {
        if (count == 0) {
            throw new IllegalArgumentException("Empty List");
        }
        T value = tail.getValue();
        tail = tail.next;
        --count;
        if (count == 0) head = null;
        else tail.next = null;
        return value;
    }

    @Override
    public DoubleNode find(T value) {
        DoubleNode<T> current = head;
        while (current != null) {
            if (current.getValue().equals(value)) {
                return current;
            } else {
                current = current.getNext();
            }
        }
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return new ListIterator();

    }

    private class ListIterator implements Iterator<T> {
        DoubleNode<T> current;
        ListIterator() {
            current = head;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            T result = current.getValue();
            current = current.getNext();
            return result;
        }
    }
}


