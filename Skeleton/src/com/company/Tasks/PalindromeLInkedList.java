package com.company.Tasks;

import java.util.Stack;

public class PalindromeLInkedList {
    public boolean isPalindrome(RemoveLinkedListElements.ListNode head) {
        if (head == null) {
            return true;
        }
        boolean resultIsPalindrome = true;
        Stack<RemoveLinkedListElements.ListNode> stack = new Stack<>();
        RemoveLinkedListElements.ListNode initialHead = head;

        while (head != null) {
            stack.push(head);
            head = head.next;
        }
        head = initialHead;
        for (int i = 1; i <= stack.size(); i++) {
            if (head.val == stack.pop().val) {
                head = head.next;
            } else {
                resultIsPalindrome = false;
                break;
            }
        }
        return resultIsPalindrome;
    }
}
