package com.company.Tasks;

public class RemoveLinkedListElements {

    public ListNode removeElements(ListNode head, int val) {
        if (head == null) {
            return null;
        }
        if (head.next == null) {
            if (head.val != val) {
                return head;
            } else return null;
        }
        if (head.val == val) {
            head = head.next;
            return removeElements(head, val);
        }
        if (head.next.val != val) {
            removeElements(head.next, val);
        } else {
            head.next = head.next.next;
            removeElements(head, val);
        }
        return head;
    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
